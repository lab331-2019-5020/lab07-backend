package se331.lab.rest.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import  org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;

@Controller
public class StudentController {
    List<Student> students;
    public StudentController(){
        this.students = new ArrayList<>();
        this.students.add(Student.builder()
                .id(1l)
                .studentId("SE-001")
                .name("Prayuth")
                .surname("The minister")
                .gpa(3.59)
                .image("https://media1.tenor.com/images/9c5d5c9962f65fc78c2f376f2cf14abc/tenor.gif?itemid=14647593")
                .penAmount(15)
                .description("The great man ever!!!")
                .build());
        this.students.add(Student.builder()
                .id(2l)
                .studentId("SE-002")
                .name("Cherprang")
                .surname("BNK48")
                .gpa(4.01)
                .image("https://media.tenor.com/images/4ba04433d2117e335b71429b59fad1b0/tenor.gif")
                .penAmount(2)
                .description("Code for Thaiand")
                .build());
        this.students.add(Student.builder()
                .id(3l)
                .studentId("SE-003")
                .name("Nobi")
                .surname("Nobita")
                .gpa(1.77)
                .image("https://pm1.narvii.com/5791/2456d9f19056c4ab82ebaa2d24770af14685572a_hq.jpg")
                .penAmount(0)
                .description("Welcome to olympic")
                .build());
    }
    @GetMapping("/students")
    public ResponseEntity getAllStudent() {
        return ResponseEntity.ok(students);
    }
    @GetMapping("/students/{id}")
    public ResponseEntity getStudentById(@PathVariable("id")Long id){
        return ResponseEntity.ok(students.get(Math.toIntExact(id-1)));
    }
    @PostMapping("/students")
    public ResponseEntity saveStudent(@RequestBody Student student){
        student.setId((long) this.students.size()+1);
        this.students.add(student);
        return ResponseEntity.ok(student);
    }
}
